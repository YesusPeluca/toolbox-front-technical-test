import React from "react";
import { Table } from "react-bootstrap";
import './style.css'

export default function TableCustom({ header, columns, rows }) {
  return (
    <div>
      <Table striped bordered hover>
        <thead>
          <tr>
            {header.map((value, key) => (
              <th key={key}>{value.label}</th>
            ))}
          </tr>
        </thead>
        <tbody>
          {rows.length > 0 ? (
            rows.map((row, key) => (
              <tr key={`table-${key}`}>
                {columns.map((headRow, key) => {
                  const rowContainer = headRow.render
                    ? headRow.render(rows)
                    : row[headRow.id];

                  return <td key={`tdColumns-${key}`}> {rowContainer}</td>;
                })}
              </tr>
            ))
          ) : (
            <tr>
              <td className="table-not-data" colSpan={columns.length}> Tabla sin datos </td>
            </tr>
          )}
        </tbody>
      </Table>
    </div>
  );
}
