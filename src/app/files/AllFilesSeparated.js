import React, { useEffect, useState } from "react";
import request from "../../helper/request";
import TableCustom from "../../components/table/Table";
import { Badge } from "react-bootstrap";
import "./style.css";

export default function AllFilesSeparated() {
  const [filesState, setFilesState] = useState();
  useEffect(() => {
    getAllFIle();
  }, []);

  const columns = [
    {
      label: "file",
      id: "file",
    },
    {
      label: "text",
      id: "text",
    },
    {
      label: "number",
      id: "number",
    },
    {
      label: "hex",
      id: "hex",
    },
  ];

  const header = [
    {
      label: "File Name",
      key: "file",
    },
    {
      label: "Text",
      key: "text",
    },
    {
      label: "Number",
      key: "number",
    },
    {
      label: "Hex",
      key: "hex",
    },
  ];

  const getAllFIle = async () => {
    try {
      const file = await request.get("file/data");

      setFilesState(file.data.data);
    } catch (error) {}
  };
  return (
    <div>
      {filesState
        ? filesState.map((value) => (
            <div
              className="container_all_files_separated"
              key={`container-table-${value.file}`}
            >
              <Badge className="all_files_separated_text" bg="secondary">{value.file}</Badge>

              <TableCustom
                className=""
                header={header}
                columns={columns}
                rows={value.lines}
              />
            </div>
          ))
        : null}
    </div>
  );
}
