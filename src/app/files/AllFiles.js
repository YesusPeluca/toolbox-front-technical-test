import React, { useEffect, useState } from "react";
import { Button, ButtonGroup } from "react-bootstrap";
import TableCustom from "../../components/table/Table";
import request from "../../helper/request";

export default function AllFiles() {
  const [filesState, setFilesState] = useState();
  const [nameFileState, setNameFileState] = useState([]);

  useEffect(() => {
    getAllFIle();
    getNameFile();
  }, []);
  const getAllFIle = async (fileName) => {
    try {
      const file = await request.get("/file/data", {
        fileName,
      });
      const data = [];

      for (let i = 0; i < file.data.data.length; i++) {
        const element = file.data.data[i].lines;
        data.push(...element);
      }
      setFilesState(data);
    } catch (error) {}
  };

  const getNameFile = async () => {
    try {
      const file = await request.get("/file");
      console.log("file :>> ", file);
      setNameFileState(file.data.data.file);
    } catch (error) {}
  };

  const selectFile = (file) => {
    console.log("file :>> ", file);
    getAllFIle(file);
  };

  const columns = [
    {
      label: "file",
      id: "file",
    },
    {
      label: "text",
      id: "text",
    },
    {
      label: "number",
      id: "number",
    },
    {
      label: "hex",
      id: "hex",
    },
  ];

  const header = [
    {
      label: "File Name",
      key: "file",
    },
    {
      label: "Text",
      key: "text",
    },
    {
      label: "Number",
      key: "number",
    },
    {
      label: "Hex",
      key: "hex",
    },
  ];

  return (
    <div>
      <ButtonGroup className="button_selected_file" aria-label="Basic example">
        <Button
          onClick={() => {
            selectFile("");
          }}
          variant="secondary"
        >
          Todos
        </Button>
        {nameFileState.map((value, key) => (
          <Button
            onClick={() => {
              selectFile(value);
            }}
            key={`button-${key}`}
            variant="secondary"
          >
            {value}
          </Button>
        ))}
      </ButtonGroup>
      {filesState ? (
        <TableCustom header={header} columns={columns} rows={filesState} />
      ) : null}
    </div>
  );
}
