import React from 'react'
import { Container, Navbar, Nav } from 'react-bootstrap'
import { Link } from 'react-router-dom'


export default function Header() {
  return (
    <Navbar bg="danger" variant="dark">
      <Container>
        <Navbar.Brand href="/">
          React Tests App
        </Navbar.Brand>
        <Nav >
          <Nav.Link ><Link to='/'>Inicio</Link></Nav.Link>
          <Nav.Link ><Link to='/separated'>Separado</Link></Nav.Link>
        </Nav>
      </Container>
    </Navbar >
  )
}
