import React from "react";
import { Container } from "react-bootstrap";
import HeaderNot from "../HeaderNot";
import "./style.css";
export default function ContentNot({ children }) {
  return (
    <>
      <HeaderNot />
      <Container expand="lg" className="container_not_protected">{children}</Container>
    </>
  );
}
