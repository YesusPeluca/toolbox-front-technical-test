import React from "react";
import { Switch, Route, BrowserRouter } from "react-router-dom";
import AllFiles from "../files/AllFiles";
import AllFilesSeparated from "../files/AllFilesSeparated";
import ContentNot from "../template/notProtectedRoutes/contentNot/ContentNot";

const notProtectedRoutes = [
  {
    Component: <AllFiles />,
    path: "/",
  },
  {
    Component: <AllFilesSeparated />,
    path: "/separated",
  },
];

const protectedRoutes = [];

const App = () => (
  <BrowserRouter>
    <Switch>
      {notProtectedRoutes.map(({ Component, path }) => (
        <Route key={path} exact path={path}>
          <ContentNot>{Component}</ContentNot>
        </Route>
      ))}
      {protectedRoutes.map(({ Component, path }) => (
        <Route key={path} exact path={path}>
          {Component}
        </Route>
      ))}
    </Switch>
  </BrowserRouter>
);

export default App;
