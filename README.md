# Code Challenge toolbox - backend

## Instalacion con docker

El sistema esta isntalado docker por ende los comandos de docker para levantar:
```sh
docker-compose up --build
```
si necesitas permisos de sistema en ubuntu se aria de esta manera
```sh
sudo docker-compose up --build
```
## instalacion sin docker

El sistema esta isntalado docker por ende los comandos de docker para levantar:
```sh
npm install
```
para iniciar el sistema se inicia  con: 

```sh
npm run start 
```
## Build

 Para generar el build de react 

```sh
npm run build 
```

## Rutas
la web se levanta a travez del puerto 3000 por defecto y tiene varias rutas

| PATCH |    DESCRIPCCION | 
| ------ | ------ | 
| / |  te muestra una vista con todos lso datos de archivos unificado y tienes botones para escoger si quieres filtrarlo
| /separated/ |   te muestra una vista donde divide todos los archivos por tablas independientes |

